﻿using System;

namespace GraphicX {
	partial class FractalForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            this.runButton = new System.Windows.Forms.Button();
            this.selectFormula = new System.Windows.Forms.ComboBox();
            this.selectColour = new System.Windows.Forms.ComboBox();
            this.selectScale = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Output = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Output)).BeginInit();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(202)))), ((int)(((byte)(70)))));
            this.runButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(202)))), ((int)(((byte)(70)))));
            this.runButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.runButton.Font = new System.Drawing.Font("Leelawadee UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.runButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.runButton.Location = new System.Drawing.Point(993, 564);
            this.runButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(265, 45);
            this.runButton.TabIndex = 0;
            this.runButton.Text = "Побудувати";
            this.runButton.UseVisualStyleBackColor = false;
            this.runButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // selectFormula
            // 
            this.selectFormula.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.selectFormula.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectFormula.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(204)));
            this.selectFormula.FormattingEnabled = true;
            this.selectFormula.Items.AddRange(new object[] {
            "z*sin(z)",
            "sin(z)/cos(z)",
            "z*sin(z)*sin(z)",
            "Жуліа",
            "tg(x)"});
            this.selectFormula.Location = new System.Drawing.Point(67, 564);
            this.selectFormula.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.selectFormula.Name = "selectFormula";
            this.selectFormula.Size = new System.Drawing.Size(265, 36);
            this.selectFormula.TabIndex = 2;
            this.selectFormula.SelectedIndexChanged += new System.EventHandler(this.selectFormula_SelectedIndexChanged);
            // 
            // selectColour
            // 
            this.selectColour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.selectColour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectColour.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(204)));
            this.selectColour.FormattingEnabled = true;
            this.selectColour.ItemHeight = 28;
            this.selectColour.Items.AddRange(new object[] {
            "Червоний",
            "Кораловий",
            "Помаранчевий",
            "Блакитний",
            "Фіолетовий"});
            this.selectColour.Location = new System.Drawing.Point(376, 564);
            this.selectColour.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.selectColour.Name = "selectColour";
            this.selectColour.Size = new System.Drawing.Size(265, 36);
            this.selectColour.TabIndex = 3;
            this.selectColour.Text = "Зелений";
            // 
            // selectScale
            // 
            this.selectScale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.selectScale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectScale.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(204)));
            this.selectScale.FormattingEnabled = true;
            this.selectScale.Items.AddRange(new object[] {
            "10%",
            "20%",
            "30%",
            "40%",
            "50%",
            "60%",
            "70%",
            "80%",
            "90%",
            "100%"});
            this.selectScale.Location = new System.Drawing.Point(681, 564);
            this.selectScale.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.selectScale.Name = "selectScale";
            this.selectScale.Size = new System.Drawing.Size(265, 36);
            this.selectScale.TabIndex = 4;
            this.selectScale.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(65, 370);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1179, 28);
            this.progressBar1.TabIndex = 5;
            this.progressBar1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(115, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(637, 46);
            this.label1.TabIndex = 9;
            this.label1.Text = "Побудова фрактальних зображень";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(177)))), ((int)(((byte)(200)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(202)))), ((int)(((byte)(70)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Leelawadee UI", 12F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(1085, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(173, 39);
            this.button1.TabIndex = 10;
            this.button1.Text = "Довідка";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(177)))), ((int)(((byte)(200)))));
            this.pictureBox2.Image = global::GraphicX.Properties.Resources.output_onlinepngtools__1_;
            this.pictureBox2.Location = new System.Drawing.Point(20, 10);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(87, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(177)))), ((int)(((byte)(200)))));
            this.pictureBox1.Image = global::GraphicX.Properties.Resources.icons8_save_64;
            this.pictureBox1.Location = new System.Drawing.Point(1006, 9);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // Output
            // 
            this.Output.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Output.Location = new System.Drawing.Point(20, 58);
            this.Output.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(1267, 494);
            this.Output.TabIndex = 1;
            this.Output.TabStop = false;
            this.Output.MouseHover += new System.EventHandler(this.Output_MouseHover);
            // 
            // FractalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(113)))), ((int)(((byte)(200)))));
            this.ClientSize = new System.Drawing.Size(1312, 639);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.selectScale);
            this.Controls.Add(this.selectColour);
            this.Controls.Add(this.selectFormula);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.runButton);
            this.Location = new System.Drawing.Point(200, 200);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FractalForm";
            this.Text = "Fracals";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Output)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		private void comboBox3_SelectedIndexChanged(object sender, EventArgs e) {
			
		}

		private void selectFormula_SelectedIndexChanged(object sender, EventArgs e) {
		}

		#endregion
		private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.PictureBox Output;
        private System.Windows.Forms.ComboBox selectFormula;
        private System.Windows.Forms.ComboBox selectColour;
        private System.Windows.Forms.ComboBox selectScale;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.PictureBox pictureBox2;
	}
}

