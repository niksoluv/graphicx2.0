﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicX {
    public partial class FractalForm : Form {
        public FractalForm() {
            InitializeComponent();
        }
        private int maxIterations = 20;
        private int max = 100;
        Bitmap mbitmap;
        List<Color> colours = new List<Color>();
        private void button1_Click(object sender, EventArgs e) {
            mbitmap = new Bitmap(Output.Width, Output.Height);
            int selectedFormula = selectFormula.SelectedIndex;
            int selectedScale = selectScale.SelectedIndex;
            double scale = (selectScale.SelectedIndex + 5) * 0.1;

            int selectedColour = selectColour.SelectedIndex;

            switch (selectedFormula) {
                case (0):
                    drawFractal(scale, colours[selectedColour]);
                    break;
                case (1):
                    drawFractal2(scale, colours[selectedColour]);
                    break;
                case 2:
                    drawFractal3(scale, colours[selectedColour]);
                    break;
                case 3:
                    drawFractal4(scale, colours[selectedColour]);
                    break;
                case 4:
                    drawFractal5(scale, colours[selectedColour]);
                    break;
                default:
                    drawFractal(scale, colours[selectedColour]);
                    break;
            }

        }

        public ComplexNumber multiplyNumbers(ComplexNumber num1, ComplexNumber num2) {
            ComplexNumber resultNum = new ComplexNumber();

            resultNum.real = num1.real * num2.real - num1.imaginary * num2.imaginary;
            resultNum.imaginary = num1.real * num2.imaginary + num1.imaginary * num2.real;

            return resultNum;
        }

        public ComplexNumber divideNumbers(ComplexNumber num1, ComplexNumber num2) {
            ComplexNumber resultNum = new ComplexNumber();

            resultNum.real = ((num1.real * num2.real + num1.imaginary * num2.imaginary) /
                num2.real * num2.real + num2.imaginary * num2.imaginary);
            resultNum.imaginary = ((num2.real * num1.imaginary - num1.imaginary * num2.real) /
                num2.real * num2.real + num2.imaginary * num2.imaginary);

            return resultNum;
        }

        public ComplexNumber getSinus(ComplexNumber num) {
            ComplexNumber res = new ComplexNumber();

            res.real = Math.Sin(num.real) * Math.Cosh(num.imaginary);

            res.imaginary = Math.Cos(num.real) * Math.Sinh(num.imaginary);

            return res;
        }

        public ComplexNumber getCosinus(ComplexNumber num) {
            ComplexNumber res = new ComplexNumber();

            res.real = Math.Cos(num.real) * Math.Sinh(num.imaginary);

            res.imaginary = Math.Sin(num.real) * Math.Cosh(num.imaginary);

            return res;
        }

        public ComplexNumber getTangens(ComplexNumber num) {
            ComplexNumber res = new ComplexNumber();

            res.real = Math.Sin(2 * num.real) / (Math.Cos(2 * num.real) +
                Math.Cosh(2 * num.imaginary));

            res.imaginary = Math.Sinh(2 * num.imaginary) / (Math.Cos(2 * num.real) +
                Math.Cosh(2 * num.imaginary));

            return res;
        }

        public void drawFractal(double scale, Color color) {
            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = 1;
            c.imaginary = 0.4;

            int w = Output.Width;
            int h = Output.Height - 1;

            int i;
            int step = Output.Width / 10;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = Output.Width;
            progressBar1.Visible = true;
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * scale * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * scale * h);


                    for (i = 0; i < maxIterations; ++i) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                        newComplexNumber = multiplyNumbers(oldComplexNumber,
                            getSinus(oldComplexNumber));
                        //newComplexNumber = multiplyNumbers(c, 
                        //    getSinus(oldComplexNumber));

                        if ((newComplexNumber.real * newComplexNumber.real +
                            newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }
                    ++i;
                    int k = (i / maxIterations) * 255;

                    mbitmap.SetPixel(x, y, i < maxIterations ?
                        Color.FromArgb(255, (color.R) / (maxIterations / (i + 1)),
                        (color.G) / (maxIterations / (i + 1)), (color.B) / (maxIterations / (i + 1))) :
                        Color.FromArgb(0, 0, 0));
                }
                if (x % step == 0) {
                    progressBar1.Value = x;
                    this.Update();
                }
            }
            progressBar1.Visible = false;
            Output.Image = mbitmap;
        }

        public void drawFractal2(double scale, Color color) {

            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = -0.70176;
            c.imaginary = -0.3842;

            int w = Output.Width;
            int h = Output.Height - 1;

            int i = 0;
            int step = Output.Width / 10;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = Output.Width;
            progressBar1.Visible = true;
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * scale * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * scale * h);

                    for (i = 0; i < maxIterations; i++) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                        newComplexNumber = divideNumbers(getSinus(oldComplexNumber),
                            getCosinus(oldComplexNumber));

                        if ((newComplexNumber.real * newComplexNumber.real +
                            newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }

                    mbitmap.SetPixel(x, y, i < maxIterations ?
                        Color.FromArgb(255, (color.R) / (maxIterations / (i + 1)), (color.G) /
                        (maxIterations / (i + 1)), (color.B) / (maxIterations / (i + 1))) :
                        Color.FromArgb(0, 0, 0));
                }
                if (x % step == 0) {
                    progressBar1.Value = x;
                    this.Update();
                }
            }
            progressBar1.Visible = false;
            Output.Image = mbitmap;
        }

        public void drawFractal3(double scale, Color color) {

            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = -0.70176;
            c.imaginary = -0.3842;

            int w = Output.Width;
            int h = Output.Height - 1;

            int i = 0;
            int step = Output.Width / 10;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = Output.Width;
            progressBar1.Visible = true;
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * scale * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * scale * h);

                    for (i = 0; i < maxIterations; i++) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                        newComplexNumber = multiplyNumbers(oldComplexNumber, multiplyNumbers(
                            getSinus(oldComplexNumber), getSinus(oldComplexNumber)));
                        if ((newComplexNumber.real * newComplexNumber.real +
                            newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }

                    mbitmap.SetPixel(x, y, i < maxIterations ?
                        Color.FromArgb(255, (color.R) / (maxIterations / (i + 1)), (color.G) /
                        (maxIterations / (i + 1)), (color.B) / (maxIterations / (i + 1))) :
                        Color.FromArgb(0, 0, 0));
                }
                if (x % step == 0) {
                    progressBar1.Value = x;
                    this.Update();
                }
            }
            progressBar1.Visible = false;
            Output.Image = mbitmap;
        }

        public void drawFractal4(double scale, Color color) {
            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = 1;
            c.imaginary = 0.2;

            int w = Output.Width;
            int h = Output.Height - 1;

            int i;
            int step = Output.Width / 10;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = Output.Width;
            progressBar1.Visible = true;
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * scale * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * scale * h);


                    for (i = 0; i < maxIterations; ++i) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                        newComplexNumber = multiplyNumbers(c,
                            getSinus(oldComplexNumber));

                        if ((newComplexNumber.real * newComplexNumber.real +
                            newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }
                    ++i;
                    int k = (i / maxIterations) * 255;

                    mbitmap.SetPixel(x, y, i < maxIterations ?
                        Color.FromArgb(255, (color.R) / (maxIterations / (i + 1)),
                        (color.G) / (maxIterations / (i + 1)), (color.B) / (maxIterations / (i + 1))) :
                        Color.FromArgb(0, 0, 0));
                }
                if (x % step == 0) {
                    progressBar1.Value = x;
                    this.Update();
                }
            }
            progressBar1.Visible = false;
            Output.Image = mbitmap;
        }

        public void drawFractal5(double scale, Color color) {
            ComplexNumber newComplexNumber = new ComplexNumber();
            ComplexNumber oldComplexNumber = new ComplexNumber();
            ComplexNumber c = new ComplexNumber();
            c.real = 1;
            c.imaginary = 0.2;

            int w = Output.Width;
            int h = Output.Height - 1;

            int i;
            int step = Output.Width / 10;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = Output.Width;
            progressBar1.Visible = true;
            for (int x = 0; x < w; ++x) {
                for (int y = 0; y < h; ++y) {

                    newComplexNumber.real = 4 * (x - w / 2) /
                        (0.5 * scale * w);
                    newComplexNumber.imaginary = 2 * (y - h / 2) /
                        (0.5 * scale * h);


                    for (i = 0; i < maxIterations; ++i) {

                        oldComplexNumber.real = newComplexNumber.real;
                        oldComplexNumber.imaginary = newComplexNumber.imaginary;

                        newComplexNumber = divideNumbers(getCosinus(oldComplexNumber),
                            multiplyNumbers(getSinus(oldComplexNumber), oldComplexNumber));

                        if ((newComplexNumber.real * newComplexNumber.real +
                            newComplexNumber.imaginary * newComplexNumber.imaginary) > max) break;
                    }
                    ++i;
                    int k = (i / maxIterations) * 255;

                    mbitmap.SetPixel(x, y, i < maxIterations ?
                        Color.FromArgb(255, (color.R) / (maxIterations / (i + 1)),
                        (color.G) / (maxIterations / (i + 1)), (color.B) / (maxIterations / (i + 1))) :
                        Color.FromArgb(0, 0, 0));
                }
                if (x % step == 0) {
                    progressBar1.Value = x;
                    this.Update();
                }
            }
            progressBar1.Visible = false;
            Output.Image = mbitmap;
        }

        private void Form1_Load(object sender, EventArgs e) {
            this.Top = 150;
            this.Left = 150;
            selectFormula.SelectedIndex = 0;
            selectColour.SelectedIndex = 0;
            selectScale.SelectedIndex = 4;
            colours.Add(Color.FromArgb(255, 0, 0));
            colours.Add(Color.FromArgb(255, 120, 40));
            colours.Add(Color.FromArgb(255, 165, 0));
            colours.Add(Color.FromArgb(0, 191, 255));
            colours.Add(Color.FromArgb(128, 0, 128));
        }

		private void pictureBox1_Click(object sender, EventArgs e) {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "png | *.png | jpeg | *.jpeg| bmp | *.bmp";
            dialog.DefaultExt = "txt";
            if (dialog.ShowDialog() == DialogResult.OK) {
                int width = Convert.ToInt32(Output.Width);
                int height = Convert.ToInt32(Output.Height);
                Bitmap bmp = new Bitmap(width, height);
                Output.DrawToBitmap(bmp, new Rectangle(0, 0, width, height));
                bmp.Save(dialog.FileName, ImageFormat.Jpeg);
            }
        }

		private void label1_Click(object sender, EventArgs e) {

		}

		private void button1_Click_1(object sender, EventArgs e) {
            FractalInfoForm ff = new FractalInfoForm();
            ff.Show();
		}

		private void pictureBox2_MouseHover(object sender, EventArgs e) {
            pictureBox2.BackColor = Color.FromArgb(98, 161, 175);
            pictureBox2.Update();
        }

		private void pictureBox2_MouseUp(object sender, MouseEventArgs e) {
            pictureBox2.BackColor = Color.FromArgb(72, 177, 200);
            pictureBox2.Update();
        }

		private void pictureBox1_MouseHover(object sender, EventArgs e) {
            pictureBox1.BackColor = Color.FromArgb(98, 161, 175);
            pictureBox1.Update();
        }

		private void pictureBox2_MouseLeave(object sender, EventArgs e) {
            pictureBox2.BackColor = Color.FromArgb(72, 177, 200);
            pictureBox2.Update();
        }

		private void pictureBox1_MouseDown(object sender, MouseEventArgs e) {
            pictureBox1.BackColor = Color.FromArgb(0, 109, 133);
            pictureBox1.Update();
        }

		private void pictureBox1_MouseClick(object sender, MouseEventArgs e) {
            
        }

		private void pictureBox2_Click(object sender, EventArgs e) {
            this.Close();
        }

		private void pictureBox1_MouseLeave(object sender, EventArgs e) {
            pictureBox1.BackColor = Color.FromArgb(72, 177, 200);
            pictureBox1.Update();
        }

		private void pictureBox1_MouseUp(object sender, MouseEventArgs e) {
            pictureBox1.BackColor = Color.FromArgb(72, 177, 200);
            pictureBox1.Update();
        }

		private void Output_MouseHover(object sender, EventArgs e) {

		}

		private void pictureBox2_MouseDown(object sender, MouseEventArgs e) {
            pictureBox2.BackColor = Color.FromArgb(0, 109, 133);
            pictureBox2.Update();
        }
	}
}
