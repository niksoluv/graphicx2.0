﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicX {
	public partial class FractalInfoForm : Form {
		public FractalInfoForm() {
			InitializeComponent();
		}

		private void pictureBox2_Click(object sender, EventArgs e) {
			this.Close();
		}

		private void pictureBox1_Click(object sender, EventArgs e) {
			this.Close();
		}

		private void pictureBox1_MouseHover_1(object sender, EventArgs e) {
			pictureBox1.BackColor = Color.FromArgb(98, 161, 175);
			pictureBox1.Update();
		}

		private void pictureBox1_MouseLeave_1(object sender, EventArgs e) {
			pictureBox1.BackColor = Color.FromArgb(72, 177, 200);
			pictureBox1.Update();
		}

		private void pictureBox1_MouseDown_1(object sender, MouseEventArgs e) {
			pictureBox1.BackColor = Color.FromArgb(0, 109, 133);
			pictureBox1.Update();
		}

		private void pictureBox1_MouseUp_1(object sender, MouseEventArgs e) {
			pictureBox1.BackColor = Color.FromArgb(72, 177, 200);
			pictureBox1.Update();
		}
	}
}
