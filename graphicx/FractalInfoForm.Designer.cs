﻿namespace GraphicX {
	partial class FractalInfoForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.Output = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.Output)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// Output
			// 
			this.Output.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Output.Image = global::GraphicX.Properties.Resources.FractalInfo;
			this.Output.Location = new System.Drawing.Point(11, 44);
			this.Output.Margin = new System.Windows.Forms.Padding(2);
			this.Output.Name = "Output";
			this.Output.Size = new System.Drawing.Size(687, 447);
			this.Output.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.Output.TabIndex = 13;
			this.Output.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(177)))), ((int)(((byte)(200)))));
			this.pictureBox1.Image = global::GraphicX.Properties.Resources.output_onlinepngtools__1_;
			this.pictureBox1.Location = new System.Drawing.Point(23, 5);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(65, 34);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 22;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
			this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown_1);
			this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave_1);
			this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover_1);
			this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp_1);
			// 
			// FractalInfoForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(113)))), ((int)(((byte)(200)))));
			this.ClientSize = new System.Drawing.Size(713, 500);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.Output);
			this.Name = "FractalInfoForm";
			this.Text = "FractalInfoForm";
			((System.ComponentModel.ISupportInitialize)(this.Output)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.PictureBox Output;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}