﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicX
{
    public partial class TheoryForm : Form
    {
        public TheoryForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.ReadOnly = false;
            richTextBox1.Clear();


            richTextBox1.Text = "\tФракталом називається фігура, яка складається з частин, які подібні до цілого. Тобто фрактали є самоподібними структурами. Фрактальна графіка заснована на математичних обчисленнях і бфзовим елементом є математичні формули.\n\tЗа способом побудови фрактали поділяються на геометричні, алгебраїчні, стохастичні та IFS - фрактали.\n";
            richTextBox1.Font = new Font(richTextBox1.Font.FontFamily, 14);
            label1.Text = "Фрактали";
            richTextBox1.Text += "\t\nАлгебраїчні фрактали\n\tАлгебраїчні фрактали є найбільшою групою фракталів. Утворюються за допомогою ітераційних процесів над простими алгебраїчними формулами.\n\tУзагальнена формула побудов фракталів:\nZ[i + 1] = f(Z[i] + C), де f – нелінійна функція, і – номер ітерації.\n\tУ нашій програмі використовуються прості тригонометричні функції, такі як sin(z), cos(z) і т. д.";


            richTextBox1.Text = richTextBox1.Text + "\tПриклад алгебрагічних Фракталів:\n\n \t\t\t\t";
            Image i = Image.FromFile("C:/Users/User/Desktop/graphicx2.0-master/graphicx/Resources/al1.jpg");
            Bitmap b = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b))
            {
                g.DrawImage(i, 0, 0, 555, 230);
            }

            //  Clipboard.Clear();
            Clipboard.SetImage(b);
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();
            Clipboard.SetText("\t\t\t\t\t\t          \t\t\t\t    Множина Мандельброта\n\n\n \t\t\t\t");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            // richTextBox1.Text += "\n\r";
            Image i1 = Image.FromFile("C:/Users/User/Desktop/graphicx2.0-master/graphicx/Resources/al2.jpg");
            Bitmap b1 = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b1))
            {
                g.DrawImage(i1, 0, 0, 555, 230);
            }

            //  Clipboard.Clear();
            Clipboard.SetImage(b1);
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();
            Clipboard.SetText("\t\t\t\t\t\t          \t\t\t    Збільшена множина Мандельброта \n\n");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Clipboard.SetText("\t\nГеометричні фрактали\n\tГеометричні фрактали отримують за допомогою ламаної, яку називають генератором.Під час кожної ітерації кожна частина вихідної ламаної замінюється на ламану - генератор у меншому масштабі.В результаті повторення цих дій утворюються геометричні фрактали.\n\tПриклади геометричних фракталів:\n\n \t\t\t\t");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Image i2 = Image.FromFile("C:/Users/User/Desktop/graphicx2.0-master/graphicx/Resources/ge1.png");
            Bitmap b2 = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b2))
            {
                g.DrawImage(i2, 0, 0, 555, 230);
            }
            Clipboard.SetImage(b2);
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();
            Clipboard.SetText("\t\t\t\t\t\t          \t\t\t\t     Крива Гільберта-Пеано\n\n\n \t\t\t\t");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Image i3 = Image.FromFile("C:/Users/User/Desktop/graphicx2.0-master/graphicx/Resources/ge2.jpg");
            Bitmap b3 = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b3))
            {
                g.DrawImage(i3, 0, 0, 555, 230);
            }
            Clipboard.SetImage(b3);
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();
            Clipboard.SetText("\t\t\t\t\t\t          \t\t\t\t    Трикутник Серпінського \n\n");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Clipboard.SetText("\t\nСтохастичні фрактали\n\tСтохастичні фрактали утворюються шляхом зміни певних параметрів у ітераційному процесі.Чтохастичні фрактали своєю формою можкть бути схожі на природні об’єкти або нагадуфати певні фізичні процеси.\n\tПриклади стохастичних фракталів:\n\n \t\t\t\t");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Image i4 = Image.FromFile("C:/Users/User/Desktop/graphicx2.0-master/graphicx/Resources/st1.jpg");
            Bitmap b4 = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b4))
            {
                g.DrawImage(i4, 0, 0, 555, 230);
            }
            Clipboard.SetImage(b4);
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();
            Clipboard.SetText("\t\t\t\t\t\t          \t\t\t\t\t\t  Плазма \n\n\n \t\t\t\t");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Image i5 = Image.FromFile("C:/Users/User/Desktop/graphicx2.0-master/graphicx/Resources/st2.jpg");
            Bitmap b5 = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b5))
            {
                g.DrawImage(i5, 0, 0, 555, 230);
            }
            Clipboard.SetImage(b5);
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();
            Clipboard.SetText("\t\t\t\t\t\t          \t\t\t\t     Стохастичний фрактал \n\n");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Clipboard.SetText("\t\nIFS фрактали\n\tIFS(Iterated Function System) – система функцій які відображають одну багатовимірну множину на іншу.Найпростіша IFS складається з функцій:\n\tX' = A*X + B*Y + C\n\tY' = D*X + E*Y + F\nТакож для побудови даних фракталів застосовуються афінні перетворення.\n\n\tПриклади фракталів побудованих за допомогою IFS:\n\n \t\t\t\t");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            Image i6 = Image.FromFile("C:/Users/User/Desktop/graphicx2.0-master/graphicx/Resources/ifs1.jpg");
            Bitmap b6 = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b6))
            {
                g.DrawImage(i6, 0, 0, 555, 230);
            }
            Clipboard.SetImage(b6);
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();
            Clipboard.SetText("\t\t\t\t\t\t          \t\t\t\t  'Дракон' Хартера-Хейтуея \n\n");
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.Paste();

            richTextBox1.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox1.ReadOnly = false;
           
            Image i = Image.FromFile("C:/Users/User/Pictures/thumb_m_11226.jpg");
            Bitmap b = new Bitmap(555, 230);
            using (Graphics g = Graphics.FromImage(b))
            {
                g.DrawImage(i, 0, 0, 555, 230);
            }

            Clipboard.SetImage(b);
          
            richTextBox1.Paste();
            
            richTextBox1.ReadOnly = true;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TheoryForm_Load(object sender, EventArgs e)
        {
            this.Left = 150;
            this.Top = 150;
        }
    }
}
